import junit.framework.TestCase;


public class FibonacciGeneratorTest extends TestCase {
	//test #1
	//test obj: EP test validate
	//input:0
	//expected output: false
	
	 public void testvalidateN001()
	 {
		 FibonacciGenerator testObj = new FibonacciGenerator();
		 assertEquals(false, testObj.validateN(0));
	 }
	
	//test #2
	//test obj: EP test validate
	//input:7
	//expected output: True
	 public void testvalidateN002()
	 {
		 FibonacciGenerator testObj = new FibonacciGenerator();
		 assertEquals(true, testObj.validateN(7));
	 }
	
	//test #3
	//test obj: EP test validate
	//input:Integer.MAX_VALUE
	//expected output:false
	 public void testvalidateN003()
	 {
		 FibonacciGenerator testObj = new FibonacciGenerator();
		 assertEquals(false, testObj.validateN(Integer.MAX_VALUE));
	 }
	//test #4
	//test obj: BVA test validate
	//input:-1
	//expected output:false
	 public void testvalidateN004()
	 {
		 FibonacciGenerator testObj = new FibonacciGenerator();
		 assertEquals(false, testObj.validateN(-1));
	 }
	//test #5
	//test obj: BVA test validate
	//input:1
	//expected output:true
	 public void testvalidateN005()
	 {
		 FibonacciGenerator testObj = new FibonacciGenerator();
		 assertEquals(true, testObj.validateN(1));
	 }
	//test #6
	//test obj: BVA test validate
	//input:Integer.MAX_VALUE-1
	//expected output:false
	public void testvalidateN006()
	 {
		FibonacciGenerator testObj = new FibonacciGenerator();
		assertEquals(false, testObj.validateN(Integer.MAX_VALUE-1));
	}
	//test #7
	//test obj: EP test calculate
	//input:0
	//expected output:Error
	public void testvalidateN007()
	 {
		FibonacciGenerator testObj = new FibonacciGenerator();
		testObj.calculateNthValue(0);
	}	
	//test #8
	//test obj: EP test calculate
	//input:7
	//expected output:13
	public void testvalidateN008()
	 {
		FibonacciGenerator testObj = new FibonacciGenerator();
		assertEquals(13, testObj.calculateNthValue(7));
	}	
	//test #9
	//test obj: EP test calculate
	//input:Integer.MAX_VALUE
	//expected output:Error
	public void testvalidateN009()
	 {
		FibonacciGenerator testObj = new FibonacciGenerator();
		testObj.calculateNthValue(Integer.MAX_VALUE);
	}	
	//test #10
	//test obj: BVA test calculate
	//input:-1
	//expected output:Error
	public void testvalidateN010()
	 {
		FibonacciGenerator testObj = new FibonacciGenerator();
		testObj.calculateNthValue(-1);
	}	
	//test #11
	//test obj: BVA test calculate
	//input:1
	//expected output:1
	public void testvalidateN011()
	 {
		FibonacciGenerator testObj = new FibonacciGenerator();
		assertEquals(1, testObj.calculateNthValue(1));
	}	
	//test #12
	//test obj: BVA test calculate
	//input: Integer.MAX_VALUE-1
	//expected output:ERROR
	public void testvalidateN012()
	 {
		FibonacciGenerator testObj = new FibonacciGenerator();
		testObj.calculateNthValue(Integer.MAX_VALUE -1);
	}
}